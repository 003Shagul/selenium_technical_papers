# Selenium #
![](https://www.devopsschool.com/blog/wp-content/uploads/2022/03/se.jpg)

## Abstracts:
---
- This paper is about Selenium.Selenium is the most popularly used freeware and open source automation tool. 
  The benefits of Selenium for Test Automation are immense. Importantly, it enables record and playback for testing web applications and can run multiple scripts across various browsers.

---
## Table of Contents :
---
- [Introduction](#introduction)
- [History](#History)
- [What is Selenium ?](#What-is-Selenium-?)
- [Selenium Architecture](#Selenium-Architecture)
- [Selenium Components](#Selenium-Components)
- [Why Selenium Over other Tools?](#Why-Selenium-Over-other-Tools?)
- [Conclusion](#Conclusion)
- [References](#References])
---
## Introduction
---
- Selenium automates browsers.
- Its Open source- Completely Free , all you need is a system and internet.
- Selenium can be executed on multiple platforms
- can be controlled by many programming languages and testing frameworks.
- Supports provided from several other open source tools like Jenkins, ANT etc.
- can be integrated with several defect tracking tools.
---
## History
---	
- Was developed by jason Huggins in 2004.He was a working on a in-house project in throughtworks.He started working on Slenium Core.
- Dan Fabulich and nelson Sproul, Created Selenium RC.
- Shinya Kastani in japan become interested in Selenium, and realised that he could wrap the code into and IDE module into the FireFox
- browser, and be able to record tests as well as play them back in the same plugin. 
---	  
## What is Selenium ?
---
- Selenium is a robust tool that supports rapid development of test automation for web-based applications.
- Can simulate a user navigating throught pages and then assert for specific marks on the pages.
- Open source, web-based testing automation tool and cross-browser compliant.
- Open source tool.
- Easy to use.

## Selenium Architecture

![](https://blog.testproject.io/wp-content/uploads/2021/05/1.-Selenium-3-Architecture.png)





---	
## Selenium Components
## The Selenium test suite comprises four main components:-
---

![](https://qatechhub.com/wp-content/uploads/2016/08/Components-of-Selenium.png)

### Selenium RC
---
- In the case of working with Selenium RC (Remote Control), one must have good knowledge of at least one programming language.
- This tool allows you to develop responsive design tests in any scripting language of your choice. 
- Server and client libraries are the two main components of Selenium RC. 
- Its architecture is complex and it has its limitations.

---	
### Selenium Web Driver
---
- Selenium WebDriver is the Successor of Selenium Remote control which has been officially deprecated.
- Selenium WebDriver is a collection of language spicific bindings to drive a browser.
- WebDriver is designed in a simpler and more concise programming interface along with addressing some limitations in the Selenium -RC API
- Also Known as Selenium 2
	    
	  Selenium1+Selenium Web Driver = Selenium2.
---
### Selenium Grid
---
- Selenium Grid can run large test suites and test that must be run in mutiple environments
- Tests can be run in parallel with simulataneous execution
- It allows for running yours tests in a distributed test execution environment.
- Used to run your tests against multiple browsers,multiple versions of browsers, and browsers running on different Operations Systems
- It reduces the time it takes for the test suite to complete a test pass.
---
### Selenium IDE
---
- If you want to create quick bug reprodution Script or create scripts to aid in automation-aided exploratory testing, then Selenium IDE is the best.
- Selenium IDE is an integrated development enviroment for Selenium Scripts.
- Its an add on with Firefox.
- It has record and play back functionality,Through it can record only records on firefox, it can be modified to run in Webdriver and RC.
- The tests can be modified, assertions can be added and test suite can be created.
---
### Features of Selenium IDE
---
- Easy record and playback
- Intelligent field selection will use IDEs,names, or XPath as needed
- Autocomplete for all common Selenium commands
- Walk through tests
- Debug and set breakpoints
- Save tests as HTML, Ruby scripts. or any others format
- support for selenium user-extensition.js file
- Option to automatically assert the title of every page.
	
---	
## Why Selenium Over other Tools?
---
- Most powerful Open source Automation tool available.
- Flexible with support to many languages
- Highly extensible
- Platform Support- Provides support on wide range of OS compared to any other tool
- Parallel Testing - Supports parallel Testing
- Usability - Easy to use.
- ALM Integration - Provides integration with several bug tracking tools.
---	
## Conclusion
--- 
- Selenium is a cost-effective and flexible tool developers can use in the automation testing of their web applications. 
- The most intriguing feature of this software is the ability to test applications across various web browsers. 
- This ensures that websites do not crash or breakdown in certain browsers.
	
---
## References
---	
- http://www.seleniumhq.org/
- https://saucelabs.com/selenium/selenium-grid
- https://www.developerfusion.com/
- https://www.browserstack.com/guide/selenium-webdriver-tutorial#:~:text=It%20supports%20a%20number%20of,%2C%20Ruby%2C%20Python%2C%20PHP.
